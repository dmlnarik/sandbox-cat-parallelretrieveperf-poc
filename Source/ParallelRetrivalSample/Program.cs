﻿namespace ParallelRetrivalSample
{
    using System;
    using System.Diagnostics;

    using BbFX.Core.Collections;
    using BbFX.Core.Storage.NoSql.Azure.DocumentDb;

    using Microsoft.Azure.Documents;
    using Microsoft.Azure.Documents.Client;

    using ParallelRetrivalSample.DocumentDbAccess;

    public class Program
    {
        private const int Repeats = 1;

        static void Main(string[] args)
        {
            var access = Initialize();

            Console.WriteLine("Starting performance testing:");

            for (int i = 0; i < Repeats; i++)
            {
                Console.WriteLine("Run {0}", i);

                try
                {
                    MeasureParallelProcessing(access);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Parallel processing has failed with an error: ");
                    Console.WriteLine(e);
                    return;
                }

                try
                {
                    MeasureAsyncProcessing(access);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Asynchronous processing has failed with an error: ");
                    Console.WriteLine(e);

                    return;
                }
            }

            Console.WriteLine("Test has finished. Press a key to continue..");

            Console.ReadKey();
        }

        private static void MeasureParallelProcessing(AttendanceStatisticsContext access)
        {
            Console.WriteLine("Parallel Processing - Started");

            Stopwatch sw = new Stopwatch();

            sw.Start();

            access.GetCourseAttendanceStatisticsDocumentsParallel();

            sw.Stop();

            Console.WriteLine("Parallel Processing - Elapsed={0}", sw.Elapsed);
        }

        private static void MeasureAsyncProcessing(AttendanceStatisticsContext access)
        {
            Console.WriteLine("Asynchronous Processing - Started");

            Stopwatch sw = new Stopwatch();

            sw.Start();

            access.GetCourseAttendanceStatisticsDocumentsAsync();

            sw.Stop();

            Console.WriteLine("Asynchronous Processing - Elapsed={0}", sw.Elapsed);
        }
        
        private static AttendanceStatisticsContext Initialize()
        {
            var connection = new DocumentDatabaseConnectionInfo()
            {
                Endpoint = new Uri("https://bbattendancetracking.documents.azure.com:443/"),
                AccessKey = "RkGLH8kc3R4eqLWW1nCXMLtJ3QcJXg8IC5i6O9CW6kpyKxm72Aaud4G5iCW84EjSLuftehPmLZR8brhHFNqLXw==",
                DatabaseName = "BbCAT",
                ConnectionMode = ConnectionMode.Direct,
                Protocol = Protocol.Tcp,
                MediaReadMode = MediaReadMode.Streamed,
                MediaRequestTimeoutInSeconds = TimeSpan.FromSeconds(60),
                RequestTimeoutInSeconds = TimeSpan.FromSeconds(90),
                ConsistencyLevel = ConsistencyLevel.Strong
            };

            var access = new AttendanceStatisticsContext(connection, new ConsoleLogWriter());
            return access;
        }
    }
}
