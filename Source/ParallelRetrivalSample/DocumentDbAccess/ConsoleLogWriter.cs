﻿namespace ParallelRetrivalSample.DocumentDbAccess
{
    using System;

    using BbFX.Core.Logging;
    public class ConsoleLogWriter : IContainerLogWriter
    {
        public void Write(string message)
        {
            Console.WriteLine("Message logged: {0}", message);
        }

        public void Write(LogEvent logEvent)
        {
            Console.WriteLine("Message logged: {0}", logEvent.Details);
        }
    }
}
