﻿namespace ParallelRetrivalSample.DocumentDbAccess
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;

    using BbFX.Core;
    using BbFX.Core.Logging;
    using BbFX.Core.Storage.NoSql.Azure.DocumentDb;

    public class AttendanceStatisticsContext : DocumentDbContextBase
    {
        /// <summary>
        /// The course attendance statistics collection identifier
        /// </summary>
        private string courseAttendanceStatisticsCollectionId = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttendanceStatisticsContext"/> class.
        /// </summary>
        /// <param name="connectionInfo">The connection information.</param>
        /// <param name="logger">The logger.</param>
        public AttendanceStatisticsContext(DocumentDatabaseConnectionInfo connectionInfo, IContainerLogWriter logger)
            : base(connectionInfo, new DocumentDbRetryPolicy(), logger)
        {
            Guard.IsNotNull(connectionInfo, "connectionInfo");

            this.courseAttendanceStatisticsCollectionId = "CourseAttendanceStatisticsData";
            this.Initialize();
        }



        /// <summary>
        /// Gets the course attendance statistics document.
        /// </summary>
        /// <param name="courseReferenceId">The course reference identifier.</param>
        /// <param name="institutionDomainId">The institution domain identifier.</param>
        /// <returns>The CourseAttendanceStatisticsDocument.</returns>
        public IEnumerable<CourseAttendanceStatisticsDocument> GetCourseAttendanceStatisticsDocumentsParallel()
        {
            return this.GetFilteredParallelAsync<CourseAttendanceStatisticsDocument>(
                x => true,
                this.courseAttendanceStatisticsCollectionId).Result;
        }

        /// <summary>
        /// Gets the course attendance statistics document.
        /// </summary>
        /// <param name="courseReferenceId">The course reference identifier.</param>
        /// <param name="institutionDomainId">The institution domain identifier.</param>
        /// <returns>The CourseAttendanceStatisticsDocument.</returns>
        public IEnumerable<CourseAttendanceStatisticsDocument> GetCourseAttendanceStatisticsDocumentsAsync()
        {
            return this.GetFilteredAsync<CourseAttendanceStatisticsDocument>(
                x => true,
                this.courseAttendanceStatisticsCollectionId).Result;
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when either the latest or transaction log collection is missing
        /// </exception>
        protected override void OnAfterInitialize()
        {
            if (!this.CollectionSelfLinksById.ContainsKey(this.courseAttendanceStatisticsCollectionId))
            {
                throw new InvalidOperationException(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "Collection {0}  missing from the Document Db.",
                        this.courseAttendanceStatisticsCollectionId));
            }
        }
    }
}
