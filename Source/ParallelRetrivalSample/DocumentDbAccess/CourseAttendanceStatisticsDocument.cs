﻿namespace ParallelRetrivalSample.DocumentDbAccess
{
    using System;
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class CourseAttendanceStatisticsDocument
    {
        /// <summary>
        /// Gets or sets the course reference identifier.
        /// </summary>
        /// <value>The course reference identifier.</value>
        [JsonProperty(PropertyName = "id")]
        public Guid CourseReferenceId { get; set; }

        /// <summary>
        /// Gets or sets the institution domain identifier.
        /// </summary>
        /// <value>The institution domain identifier.</value>
        public Guid InstitutionDomainId { get; set; }

        /// <summary>
        /// Gets or sets the course start date.
        /// </summary>
        /// <value>The course start date.</value>
        public DateTime CourseStartDate { get; set; }

        /// <summary>
        /// Gets or sets the course end date.
        /// </summary>
        /// <value>The course end date.</value>
        public DateTime CourseEndDate { get; set; }

        /// <summary>
        /// Gets or sets the student enrollment count.
        /// </summary>
        /// <value>The student enrollment count.</value>
        public int StudentEnrollmentCount { get; set; }

        /// <summary>
        /// Gets or sets the no classes missed student count.
        /// </summary>
        /// <value>The no classes missed student count.</value>
        public int NoClassesMissedStudentCount { get; set; }

        /// <summary>
        /// Gets or sets up to two classes missed student count.
        /// </summary>
        /// <value>Up to two classes missed student count.</value>
        public int UpToTwoClassesMissedStudentCount { get; set; }

        /// <summary>
        /// Gets or sets the two plus classes missed student count.
        /// </summary>
        /// <value>The two plus classes missed student count.</value>
        public int TwoPlusClassesMissedStudentCount { get; set; }

        /// <summary>
        /// Gets or sets the course student attendance statistics documents.
        /// </summary>
        /// <value>The course student attendance statistics documents.</value>
        public List<CourseStudentAttendanceStatisticsDocument> CourseStudentAttendanceStatisticsDocuments { get; set; }
    }
}
