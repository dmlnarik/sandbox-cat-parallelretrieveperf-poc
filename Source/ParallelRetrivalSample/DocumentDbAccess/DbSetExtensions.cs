﻿namespace ParallelRetrivalSample.DocumentDbAccess
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public static class DbSetExtensions
    {
        /// <summary>
        /// Gets the entity set.
        /// </summary>
        /// <typeparam name="TEntity">The type of the t entity.</typeparam>
        /// <param name="inputSet">The input set.</param>
        /// <param name="includes">The includes.</param>
        /// <returns>TEntity of Type Class</returns>
        public static IQueryable<TEntity> GetEntitySet<TEntity>(this DbSet<TEntity> inputSet, List<string> includes) where TEntity : class
        {
            var entitySet = inputSet.AsQueryable();
            if (includes != null)
            {
                foreach (var include in includes)
                {
                    entitySet = entitySet.Include(include);
                }
            }
            return entitySet;
        }
    }
}
