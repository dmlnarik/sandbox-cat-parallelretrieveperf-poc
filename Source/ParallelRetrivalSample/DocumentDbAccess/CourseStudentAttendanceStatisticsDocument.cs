﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelRetrivalSample.DocumentDbAccess
{
    public class CourseStudentAttendanceStatisticsDocument
    {
        /// <summary>
        /// Gets or sets the person reference identifier.
        /// </summary>
        /// <value>The person reference identifier.</value>
        public Guid PersonReferenceId { get; set; }

        /// <summary>
        /// Gets or sets the absence count.
        /// </summary>
        /// <value>The absence count.</value>
        public int AbsenceCount { get; set; }

        /// <summary>
        /// Gets or sets the presence count.
        /// </summary>
        /// <value>The presence count.</value>
        public int PresenceCount { get; set; }
    }
}
